package com.bingo.order.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @SentinelResource("queryGoods")
    public String queryGoods() {
        return "order/queryGoods";
    }
}
