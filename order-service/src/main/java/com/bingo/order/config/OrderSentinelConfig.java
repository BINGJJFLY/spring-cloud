package com.bingo.order.config;

import com.alibaba.csp.sentinel.adapter.servlet.CommonFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
public class OrderSentinelConfig {

    @Bean
    public FilterRegistrationBean sentinelFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter((Filter) new CommonFilter());
        registration.addUrlPatterns(new String[]{"/*"});

        registration.addInitParameter("WEB_CONTEXT_UNIFY", "false");
        registration.setName("sentinelFilter");
        registration.setOrder(1);
        return registration;
    }
}
