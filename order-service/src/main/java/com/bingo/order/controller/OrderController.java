package com.bingo.order.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.bingo.order.config.CommonConfig;
import com.bingo.order.config.PatternConfig;
import com.bingo.order.service.OrderService;
import com.bingo.user.api.UserApi;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    PatternConfig patternConfig;
    @Autowired
    CommonConfig commonConfig;
    @Autowired
    UserApi userApi;
    @Autowired
    OrderService orderService;

    @GetMapping({"/now"})
    public String now() {
        return this.commonConfig
                .getLabel() +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern(this.patternConfig.getDateformat()));
    }

    @GetMapping({"/order"})
    public String order(@RequestParam("id") Long id) {
        return this.userApi.getUsername(id);
    }


    @SentinelResource("/hot")
    @GetMapping({"/order/query"})
    public String orderQuery(@RequestParam("id") Long id) {
        return "/order/query";
    }

    @GetMapping({"/order/update"})
    public String orderUpdate() {
        this.orderService.queryGoods();
        return "/order/update";
    }

    @GetMapping({"/order/save"})
    public String orderSave() {
        this.orderService.queryGoods();
        return "/order/save";
    }

    @GetMapping({"/cluster"})
    public String cluster(@RequestParam("id") Long id) {
        return "cluster-" + id;
    }
}
