package com.bingo.user.api;

import com.bingo.user.sentinel.UserApiFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "user-service", fallbackFactory = UserApiFallbackFactory.class)
public interface UserApi {

    @GetMapping({"/user/getUsername"})
    String getUsername(@RequestParam("id") Long paramLong);
}