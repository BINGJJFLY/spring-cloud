package com.bingo.user.sentinel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserApiSentinelConfig {

    /**
     * Feign整合Sentinel，降级后处理逻辑类
     *
     * @return
     */
    @Bean
    public UserApiFallbackFactory userApiFallbackFactory() {
        return new UserApiFallbackFactory();
    }
}
