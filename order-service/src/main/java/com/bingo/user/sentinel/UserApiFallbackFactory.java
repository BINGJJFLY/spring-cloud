package com.bingo.user.sentinel;

import com.bingo.user.api.UserApi;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserApiFallbackFactory implements FallbackFactory<UserApi> {

    public UserApi create(Throwable throwable) {
        return id -> {
            log.error("查询用户异常,id={}", id, throwable);
            return "降级用户-" + id;
        };
    }
}