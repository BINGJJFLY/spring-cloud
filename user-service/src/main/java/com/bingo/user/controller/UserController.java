package com.bingo.user.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/user"})
public class UserController {

    @GetMapping({"/getUsername"})
    public String getUsername(@RequestParam("id") Long id) throws InterruptedException {
        if (Long.valueOf(1).equals(id)) {
            Thread.sleep(60);
        } else if (Long.valueOf(2).equals(id)) {
            throw new RuntimeException("抛异常了");
        }
        return "hello springCloud " + id;
    }
}